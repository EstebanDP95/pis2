
var tmpFormulario = Vue.component('creardocumentos',{
    template:'#creardocumentosid',
});


var tmpListaDoc = Vue.component('listadoc',{
    template:'#lista-documentos',
    data(){
        return{
            documentos: [
				{
					nombre: 'documento.js', 
					icono: '',
					descripcion: 1
				},{
					nombre: 'documento.html', 
					icono: '',
					descripcion: 2
				}
            ],
            
			listaIconos:[
				{
					archivo:'c',
					icono:'devicon-c-plain colored'
				},{
					archivo:'gcc',
					icono:'devicon-cplusplus-plain colored'
				},{
					archivo:'html',
					icono:'devicon-html5-plain colored'
				},{
					archivo:'js',
					icono:'devicon-javascript-plain colored'
				},{
					archivo:'css',
					icono:'devicon-css3-plain colored'
				},{
					archivo:'c',
					icono:'devicon-c-plain colored'
				},{
					archivo:'java',
					icono:'devicon-java-plain colored'
				},{
					archivo:'py',
					icono:'devicon-python-plain colored'
				}
			]

        }
	},
	methods:{
		creardocumentos(){
			/**peticion a la base de datos */
			var archivo;
			this.documentos.forEach(doc => {
				archivo = doc.nombre.split('.');
				doc.icono = this.listaIconos.find( name => name.archivo === archivo[1]).icono;
			});

		}
	}
});
/**
 * Componente principal
 */

var app = new Vue({
    el:'#main',  
    data(){
        return{
            mostrarformulario : false,
            mostrarListado: true,
            options: [
                {text: 'documento.py', value: 1},
                {text: 'documento.c', value: 2}
            ],
            num: 3
        }
    },
    methods:{
        verComponenteformulario(){
            this.mostrarformulario = !this.mostrarformulario;
            this.mostrarListado = !this.mostrarListado;
		}	
	}

});