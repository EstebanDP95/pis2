

Vue.component('crearproyecto',{
    template:'#crearproyectoid',
    data(){
        return{
            formulario: false
        }
    }
});

/**
 * Componente principal
 */

var app = new Vue({
    el:'#main',  
    data(){
        return{
            formulario : true,
        }
    },
    methods:{
        verformulario(){
            formulario = !formulario;
            console.log(formulario);
        }
    }
});