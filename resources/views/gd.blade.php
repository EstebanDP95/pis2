<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="{!! asset('img/gp_ico.ico') !!}"/>
        <link rel="stylesheet" href="/css/index.css" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat&display=swap" rel="stylesheet">
        <title>Gestion de Proyectos</title>
    </head>
    <body>
        <div id="Header">
            @include('Header/Header')
        </div>
        <div class="contenedor">
            <?php
                // http://localhost:8080/gd?name=Esteban
                echo '¡Hola ' . htmlspecialchars($_GET["name"]) . '!';
                // echo <h1></h1>
                // echo '<span class="nombre">Nombre:</span>'
            ?>
            <div class="formulario">
                    <span class="nombre">Nombre:</span>
                    <input type="text" class="txtNombre textInput"> </input><br>
                    <span class="correo">Correo:</span>
                    <input type="mail" class="txtCorreo textInput"> </input><br>
                    <span class="password">Contraseña:</span>
                    <input type="password" class="txtPassw textInput"> </input>
                    <input type="submit" value="Registrar" class="btnRegistro textInput"></input>
            </div>
        </div>
        <div id="footer"></div>
    </body>
</html>
