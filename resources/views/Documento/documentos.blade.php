<!-- HEAD --->
<head>
    <!--Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!--Styles-->
    <link href="/css/Documentos/Documentos.css" rel="stylesheet">

    <!--Styles Button-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/Documentos/listadoc.css') }}" rel="stylesheet">
</head>
<body>
    <div id="nav">
        @include('Header/Header')
    </div>
    <div id="main">
        <div id="side-nav">
            <button type="submit" class="btn btn-primary" @click="tmpListaDoc.creardocumentos()">
                Agregar documentos
            </button>
            <!--Agregar funciones de documentos: Eliminar, crear, Asignar Tareas, etc-->
        </div>
        <div id="contenido">
        
            <div id="listado-proyectos" class="ddd">
                <!--Agregar listado de documentos-->
                <!--Crear componentes para cada documento-->
                <!--Las propiedades aparecen al seleccionar el documento-->
                <creardocumentos v-show="mostrarformulario"></creardocumentos>
                <listadoc v-show="true"></listadoc>
            </div>
        </div>
    </div>
</body>

<!--Templates-->

<template id="lista-documentos">
    @include('Documento/listaDocumentos')
    <div class="lista">
        @yield('documentos')
    </div>
</template>

<template id="crearDocumentosid">
    @include('Documento/crearDocumentos')
    <div class="form">
        @yield('crearDocumento')
    </div>
</template>

<!--Scripts-->

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="/js/Documentos/documentos.js"></script>
@yield('incluir_css')
