<!-- HEAD --->
<head>
    <!--Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <!--Styles-->
    <link href="/css/Proyecto/Proyecto.css" rel="stylesheet">
    <!--Styles Button-->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="nav">
        @include('Header/Header')
    </div>
    <div id="main">
        <div id="side-nav">
            <button type="submit" class="btn btn-primary" @click="verformulario()">
                Agregar proyecto
            </button>
            <!--Agregar listado de operaciones sobre el proyecto:Agregar,Eliminar,Propiedades,etc-->
        </div>
        <div id="contenedor">  
            
            <div id="listado-proyectos">
                <!--Agregar listado de proyectos-->
                <!--Crear componentes para cada proyectos-->
                <!--Las propiedades aparecen al seleccionar el proyecto-->
                <crearproyecto v-show="formulario"></crearproyecto>            
            </div>
          
        </div>
    </div>
</body>
<!--Templates-->
<template id="crearproyectoid">
    @include('Proyecto/crearProyecto')
    <div class="formulario">
        @yield('formularioproyecto')
    </div>
</template>


<!--Scripts-->
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="/js/Proyecto/Proyecto.js"></script>
