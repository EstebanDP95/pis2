<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Proyecto', function(){
    return view('/Proyecto/proyecto');
});

Route::get('/Documentos', function(){
    return view('/Documento/documentos');
});


Route::get('/crearProyecto', function(){
    return view('/Proyecto/crearProyecto');
});


Route::get('validacion','ValidacionController@validacion');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
