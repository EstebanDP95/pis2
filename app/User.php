<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Contracts\Auth\Authenticatable;


/**
 * @MongoDB\Document(collection="users")
 * @MongoDBUnique(fields="email")
 */
class User extends Eloquent implements Authenticatable
{
    /**
     * @MongoDB\Id
     */
    use AuthenticableTrait;
    use Notifiable;

    protected $connection = 'mongodb';

    protected $fillable = [
        'name','email','password',
    ];


    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank()
     * @Assert\Email()
     */

    /**
     * @MongoDB\Field(type="string")
     * @Assert\NotBlank()
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getId()
    {
        return $this->id;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }
}